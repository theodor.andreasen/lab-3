package no.uib.inf101.terminal;

public class CmdEcho implements Command{
    
    @Override
    public String getName() {
        return "echo";
    }
    @Override
    public String run(String[]args){
        String message = "";
    for (String verdi : args) {
        message +=verdi + " ";
    }
    return message;
    }
}
